require = require('@std/esm')(module, { esm: 'mjs', cjs: true });

const OpenXum = require('../../../../lib/openxum-core/').default;

describe('Patterns', () => {
  test('Pattern number equals to 5', () => {
    expect(OpenXum.Kikotsoka.Patterns.length).toBe(5);
  });
  test('Pattern (level 1) have 2 variations', () => {
    expect(OpenXum.Kikotsoka.Patterns[0].length).toBe(2);
  });
  test('Pattern (level 2) have 4 variations', () => {
    expect(OpenXum.Kikotsoka.Patterns[1].length).toBe(4);
  });
  test('Pattern (level 3) have 4 variations', () => {
    expect(OpenXum.Kikotsoka.Patterns[2].length).toBe(4);
  });
  test('Pattern (level 4) have 4 variations', () => {
    expect(OpenXum.Kikotsoka.Patterns[3].length).toBe(4);
  });
  test('Pattern (level 5) have 4 variations', () => {
    expect(OpenXum.Kikotsoka.Patterns[4].length).toBe(4);
  });
});

describe('Coordinates', () => {
  test('to string', () => {
    const coordinates = new OpenXum.Kikotsoka.Coordinates(0,0);
    const str = coordinates.to_string();

    expect(str).toEqual(expect.any(String));
    expect(str).toBe('A1');
  });
});