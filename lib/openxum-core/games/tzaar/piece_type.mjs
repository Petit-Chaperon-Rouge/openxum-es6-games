"use strict";

let PieceType = {TZAAR: 0, TZARRA: 1, TOTT: 2};

export default PieceType;