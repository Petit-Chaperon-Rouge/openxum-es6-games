"use strict";

import Color from './color.mjs';
import Coordinates from './coordinates.mjs';
import OpenXum from '../../openxum/index.mjs';
import Phase from './phase.mjs';

class Move extends OpenXum.Move {
  constructor(t, c, to) {
    super();
    this._type = t;
    this._color = c;
    this._to = to;
  }

// public methods
  color() {
    return this._color;
  }

  decode(str) {
    this._color = str[0] === 'B' ? Color.BLACK : Color.WHITE;
    if (str.length === 2) {
      this._type = Phase.PASS;
      this._to = null;
    } else {
      this._type = Phase.PUT_PIECE;
      if (str.length === 3) {
        this._to = new Coordinates(str.charCodeAt(1) - 'A'.charCodeAt(0),
          str.charCodeAt(2) - '1'.charCodeAt(0));
      } else {
        this._to = new Coordinates(str.charCodeAt(1) - 'A'.charCodeAt(0),
          (str.charCodeAt(2) - '0'.charCodeAt(0)) * 10 + (str.charCodeAt(3) - '0'.charCodeAt(0)) - 1);
      }
    }
  }

  encode() {
    if (this._type === Phase.PASS) {
      return (this._color === Color.BLACK ? 'B' : 'W') + '-';
    } else {
      return (this._color === Color.BLACK ? 'B' : 'W') + this._to.to_string();
    }
  }

  to() {
    return this._to;
  }

  to_object() {
    return {type: this._type, color: this._color, to: this._to};
  }

  to_string() {
    if (this._type === Phase.PASS) {
      return 'PASS';
    } else {
      return 'PUT ' + (this._color === Color.BLACK ? 'black' : 'white') + ' PIECE at ' +
        this._to.to_string();
    }
  }

  type() {
    return this._type;
  }
}

export default Move;